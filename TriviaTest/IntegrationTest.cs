﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApprovalTests;
using ApprovalTests.Reporters;
using NUnit.Framework;
using Trivia_csharp.Init;

namespace TriviaTest
{
    public class IntegrationTest
    {
        [Test]
        [UseReporter(typeof(DiffReporter))]
        public void RefactoringTests()
        {
            var output = new StringBuilder();
            Console.SetOut(new StringWriter(output));
            Game game = new Game();

            game.add("Bob");
            Assert.IsFalse(game.isPlayable());
            game.add("Bill");
            Assert.IsTrue(game.isPlayable());
            game.add("Clara");
            Assert.IsTrue(game.isPlayable());

            for (var i = 0; i < 100; i++)
            {
                game.roll(i%10);
                bool result;
                if (i%4 == 0)
                {
                    result = game.wrongAnswer();
                }
                else
                {
                    result = game.wasCorrectlyAnswered();
                }
                if (!result)
                    break;
            }

            //GameRunner.Main(new string[0]);
            Assert.AreEqual(@"Bob was added
They are player number 1
Bill was added
They are player number 2
Clara was added
They are player number 3
Bob is the current player
They have rolled a 0
Bob's new location is 0
The category is Pop
Pop Question 0
Question was incorrectly answered
Bob was sent to the penalty box
Bill is the current player
They have rolled a 1
Bill's new location is 1
The category is Science
Science Question 0
Answer was correct!!!!
Bill now has 1 Gold Coins.
Clara is the current player
They have rolled a 2
Clara's new location is 2
The category is Sports
Sports Question 0
Answer was correct!!!!
Clara now has 1 Gold Coins.
Bob is the current player
They have rolled a 3
Bob is getting out of the penalty box
Bob's new location is 3
The category is Rock
Rock Question 0
Answer was correct!!!!
Bob now has 1 Gold Coins.
Bill is the current player
They have rolled a 4
Bill's new location is 5
The category is Science
Science Question 1
Question was incorrectly answered
Bill was sent to the penalty box
Clara is the current player
They have rolled a 5
Clara's new location is 7
The category is Rock
Rock Question 1
Answer was correct!!!!
Clara now has 2 Gold Coins.
Bob is the current player
They have rolled a 6
Bob is not getting out of the penalty box
Bill is the current player
They have rolled a 7
Bill is getting out of the penalty box
Bill's new location is 0
The category is Pop
Pop Question 1
Answer was correct!!!!
Bill now has 2 Gold Coins.
Clara is the current player
They have rolled a 8
Clara's new location is 3
The category is Rock
Rock Question 2
Question was incorrectly answered
Clara was sent to the penalty box
Bob is the current player
They have rolled a 9
Bob is getting out of the penalty box
Bob's new location is 0
The category is Pop
Pop Question 2
Answer was correct!!!!
Bob now has 2 Gold Coins.
Bill is the current player
They have rolled a 0
Bill is not getting out of the penalty box
Clara is the current player
They have rolled a 1
Clara is getting out of the penalty box
Clara's new location is 4
The category is Pop
Pop Question 3
Answer was correct!!!!
Clara now has 3 Gold Coins.
Bob is the current player
They have rolled a 2
Bob is not getting out of the penalty box
Question was incorrectly answered
Bob was sent to the penalty box
Bill is the current player
They have rolled a 3
Bill is getting out of the penalty box
Bill's new location is 3
The category is Rock
Rock Question 3
Answer was correct!!!!
Bill now has 3 Gold Coins.
Clara is the current player
They have rolled a 4
Clara is not getting out of the penalty box
Bob is the current player
They have rolled a 5
Bob is getting out of the penalty box
Bob's new location is 5
The category is Science
Science Question 2
Answer was correct!!!!
Bob now has 3 Gold Coins.
Bill is the current player
They have rolled a 6
Bill is not getting out of the penalty box
Question was incorrectly answered
Bill was sent to the penalty box
Clara is the current player
They have rolled a 7
Clara is getting out of the penalty box
Clara's new location is 11
The category is Rock
Rock Question 4
Answer was correct!!!!
Clara now has 4 Gold Coins.
Bob is the current player
They have rolled a 8
Bob is not getting out of the penalty box
Bill is the current player
They have rolled a 9
Bill is getting out of the penalty box
Bill's new location is 0
The category is Pop
Pop Question 4
Answer was correct!!!!
Bill now has 4 Gold Coins.
Clara is the current player
They have rolled a 0
Clara is not getting out of the penalty box
Question was incorrectly answered
Clara was sent to the penalty box
Bob is the current player
They have rolled a 1
Bob is getting out of the penalty box
Bob's new location is 6
The category is Sports
Sports Question 1
Answer was correct!!!!
Bob now has 4 Gold Coins.
Bill is the current player
They have rolled a 2
Bill is not getting out of the penalty box
Clara is the current player
They have rolled a 3
Clara is getting out of the penalty box
Clara's new location is 2
The category is Sports
Sports Question 2
Answer was correct!!!!
Clara now has 5 Gold Coins.
Bob is the current player
They have rolled a 4
Bob is not getting out of the penalty box
Question was incorrectly answered
Bob was sent to the penalty box
Bill is the current player
They have rolled a 5
Bill is getting out of the penalty box
Bill's new location is 5
The category is Science
Science Question 3
Answer was correct!!!!
Bill now has 5 Gold Coins.
Clara is the current player
They have rolled a 6
Clara is not getting out of the penalty box
Bob is the current player
They have rolled a 7
Bob is getting out of the penalty box
Bob's new location is 1
The category is Science
Science Question 4
Answer was correct!!!!
Bob now has 5 Gold Coins.
Bill is the current player
They have rolled a 8
Bill is not getting out of the penalty box
Question was incorrectly answered
Bill was sent to the penalty box
Clara is the current player
They have rolled a 9
Clara is getting out of the penalty box
Clara's new location is 11
The category is Rock
Rock Question 5
Answer was correct!!!!
Clara now has 6 Gold Coins.
", output.ToString());
        }
    }
}


