﻿using System;
using System.Collections.Generic;
using System.Linq;
using Trivia;

namespace Trivia_csharp.Init
{
    public class Game
    {


        List<Player> players = new List<Player>();

        int[] places = new int[6];

        bool[] inPenaltyBox = new bool[6];

        Category popQuestions = new Category("Pop");
        Category scienceQuestions = new Category("Science");
        Category sportsQuestions = new Category("Sports");
        Category rockQuestions = new Category("Rock");

        int currentPlayer = 0;
        bool isGettingOutOfPenaltyBox;

        public Game()
        {
            popQuestions.CreateCategoryQuestions();
            scienceQuestions.CreateCategoryQuestions();
            sportsQuestions.CreateCategoryQuestions();
            rockQuestions.CreateCategoryQuestions();
        }

        public bool isPlayable()
        {
            return (howManyPlayers() >= 2);
        }

        public bool add(String playerName)
        {
            players.Add(new Player(playerName));
            places[howManyPlayers()] = 0;
            players[howManyPlayers() - 1].Purses = 0;
            inPenaltyBox[howManyPlayers()] = false;

            Console.WriteLine(playerName + " was added");
            Console.WriteLine("They are player number " + players.Count);
            return true;
        }

        public int howManyPlayers()
        {
            return players.Count;
        }

        public void roll(int roll)
        {
            Console.WriteLine(players[currentPlayer].Name + " is the current player");
            Console.WriteLine("They have rolled a " + roll);

            if (inPenaltyBox[currentPlayer])
            {
                if (roll % 2 != 0)
                {
                    isGettingOutOfPenaltyBox = true;

                    Console.WriteLine(players[currentPlayer].Name + " is getting out of the penalty box");
                    GoToNextPlace(roll);
                }
                else
                {
                    Console.WriteLine(players[currentPlayer].Name + " is not getting out of the penalty box");
                    isGettingOutOfPenaltyBox = false;
                }

            }
            else
            {

                GoToNextPlace(roll);
            }

        }

        private void GoToNextPlace(int roll)
        {
            places[currentPlayer] = places[currentPlayer] + roll;
            if (places[currentPlayer] > 11) places[currentPlayer] = places[currentPlayer] - 12;

            Console.WriteLine(players[currentPlayer].Name
                              + "'s new location is "
                              + places[currentPlayer]);
            Console.WriteLine("The category is " + currentCategory(places[currentPlayer]).Name);
            askQuestion();
        }

        private void askQuestion()
        {
            var category = currentCategory(places[currentPlayer]);
            Console.WriteLine(category.Questions.First());
            category.Questions.RemoveFirst();
        }

        private Category currentCategory(int place)
        {
            switch (place)
            {
                case 0:
                case 4:
                case 8:
                    return popQuestions;
                case 1:
                case 5:
                case 9:
                    return scienceQuestions;
                case 2:
                case 6:
                case 10:
                    return sportsQuestions;
                default:
                    return rockQuestions;
            }
        }

        public bool wasCorrectlyAnswered()
        {
            if (inPenaltyBox[currentPlayer])
            {
                if (isGettingOutOfPenaltyBox)
                {
                    return PlayerWinsTurn();
                }
                else
                {
                    MoveToNextPlayer();
                    return true;
                }
            }
            else
            {

                return PlayerWinsTurn();
            }
        }

        private bool PlayerWinsTurn()
        {
            Console.WriteLine("Answer was correct!!!!");
            players[currentPlayer].Purses++;
            Console.WriteLine(players[currentPlayer].Name
                              + " now has "
                              + players[currentPlayer].Purses
                              + " Gold Coins.");

            bool winner = didPlayerWin();
            MoveToNextPlayer();

            return winner;
        }

        private void MoveToNextPlayer()
        {
            currentPlayer++;
            if (currentPlayer == players.Count) currentPlayer = 0;
        }

        public bool wrongAnswer()
        {
            Console.WriteLine("Question was incorrectly answered");
            Console.WriteLine(players[currentPlayer].Name + " was sent to the penalty box");
            inPenaltyBox[currentPlayer] = true;

            MoveToNextPlayer();
            return true;
        }


        private bool didPlayerWin()
        {
            return !(players[currentPlayer].Purses == 6);
        }
    }

}
