﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia
{
    public class Category
    {
        public Category(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
        public LinkedList<string> Questions { get; } = new LinkedList<string>();

        public static string createCategoryQuestion(int index, string categoryName)
        {
            return $"{categoryName} Question {index}";
        }

        public void CreateCategoryQuestions()
        {
            for (int i = 0; i < 50; i++)
            {
                Questions.AddLast(createCategoryQuestion(i, Name));
            }
        }
    }
}
