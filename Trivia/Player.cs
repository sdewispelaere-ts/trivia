﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia
{
    public class Player
    {
        public Player(string playerName)
        {
            Name = playerName;
        }

        public string Name { get; set; }

        public int Purses { get; set; }

        public int Place { get; set; }
    }
}
